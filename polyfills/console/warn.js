(function(console) {
  console.warn = console.warn || console.log;
})(this.console = this.console || {});

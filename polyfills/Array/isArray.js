if(!('isArray' in Array)) {
  (function(toString) {
    Object.defineProperty(Array, 'isArray', {
      configurable: true,
      value: function(object) {
        return toString.call(object) === '[object Array]';
      },
      writable: true
    });
  })(Object.prototype.toString);
}

if(!('contains' in Array.prototype)) {
  Array.prototype.contains = Array.prototype.includes;
}

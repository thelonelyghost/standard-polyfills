if(!('includes' in Array.prototype)) {
  Array.prototype.includes = function(searchElement /*, fromIndex*/ ) {
    'use strict';

    var object = Object(this),
      length = parseInt(object.length) || 0,
      startIndex = parseInt(arguments[1]) || 0,
      index,
      currentElement;

    if(startIndex >= 0) {
      index = startIndex;
    }
    else {
      index = length + startIndex;
      if(index < 0) index = 0;
    }

    while(index < length) {
      currentElement = object[index];
      if(searchElement === currentElement) return true;
      if(searchElement !== currentElement && currentElement !== currentElement) return true;
      index++;
    }

    return false;
  }
}

if(!('fill' in Array.prototype)) {
  Array.prototype.fill = function(value) {
    if(this === undefined || this === null) {
      throw new TypeError(this + ' is not an object');
    }

    var arraylike = Object(this),
      length = Math.max(Math.min(arraylike.length, 9007199254740991), 0) || 0,
      relativeStart = 1 in arguments ? parseInt(Number(arguments[1]), 10) || 0 : 0,
      relativeEnd = 2 in arguments && arguments[2] !== undefined ? parseInt(Number(arguments[2]), 10) || 0 : length;

    relativeStart = relativeStart < 0 ? Math.max(length + relativeStart, 0) : Math.min(relativeStart, length);
    relativeEnd = relativeEnd < 0 ? Math.max(length + arguments[2], 0) : Math.min(relativeEnd, length);

    while(relativeStart < relativeEnd) {
      arraylike[relativeStart] = value;

      ++relativeStart;
    }

    return arraylike;
  };
}

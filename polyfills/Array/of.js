if(!('of' in Array)) {
  (function(slice) {
    Object.defineProperty(Array, 'of', {
      configurable: true,
      value: function() {
        return slice.call(arguments);
      },
      writable: true
    });
  })(Array.prototype.slice);
}

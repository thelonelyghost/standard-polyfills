if(!('assign' in Object)) {
  Object.defineProperty(Object, 'assign', {
    configurable: true,
    value: function(target, source) {
      var index,
        key,
        src;
      for(index=1; index < arguments.length; ++index) {
        src = arguments[index];

        for(key in src) {
          if(Object.prototype.hasOwnProperty.call(src, key)) {
            target[key] = src[key];
          }
        }
      }

      return target;
    },
    writable: true
  });
}

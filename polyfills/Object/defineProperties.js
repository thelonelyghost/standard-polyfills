if(!('defineProperties' in Object)) {
  Object.defineProperty(Object, 'defineProperties', {
    configurable: true,
    value: function(object, descriptors) {
      for(var property in descriptors) {
        Object.defineProperty(object, property, descriptors[property]);
      }

      return object;
    },
    writable: true
  });
}

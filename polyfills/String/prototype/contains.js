if(!('contains' in String.prototype)) {
  String.prototype.contains = String.prototype.includes;
}

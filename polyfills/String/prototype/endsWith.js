if(!('endsWith' in String.prototype)) {
  String.prototype.endsWith = function(str /*, index*/ ) {
    var index = arguments.length < 2 ? this.length : arguments[1],
      foundIndex = this.lastIndexOf(str);
    return foundIndex !== -1 && foundIndex === index - str.length;
  };
}

if(!('startsWith' in String.prototype)) {
  String.prototype.startsWith = function(str /*, index */ ) {
    var index = arguments.length < 2 ? 0 : arguments[1];

    return this.slice(index).indexOf(str) === 0;
  };
}

;'use strict';

var gulp = require('gulp'),
  concat = require('gulp-concat'),
  uglify = require('gulp-uglify'),
  size = require('gulp-size'),
  clean = require('gulp-clean'),
  jshint = require('gulp-jshint');

gulp.task('clean', function() {
  gulp.src(['build/*', 'dist/*'], { read: false })
    .pipe(clean());
});

gulp.task('Array', function() {
  gulp.src(['polyfills/Array/*.js', 'polyfills/Array/prototype/(!contains)*.js', 'polyfills/Array/prototype/contains.js'])
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(concat('Array.js'))
    .pipe(size())
    .pipe(gulp.dest('build'));
});

gulp.task('Promise', function() {
  gulp.src('polyfills/Promise/**/*.js')
    .pipe(concat('Promise.js'))
    .pipe(size())
    .pipe(gulp.dest('build'));
});

gulp.task('String', function() {
  gulp.src(['polyfills/String/**/(!contains)*.js', 'polyfills/String/prototype/contains.js'])
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(concat('String.js'))
    .pipe(size())
    .pipe(gulp.dest('build'));
});

gulp.task('Object', function() {
  gulp.src(['polyfills/Object/defineProperty.js', 'polyfills/Object/*.js'])
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(concat('Object.js'))
    .pipe(size())
    .pipe(gulp.dest('build'));
});

gulp.task('Number', function() {
  gulp.src('polyfills/Number/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(concat('Number.js'))
    .pipe(size())
    .pipe(gulp.dest('build'));
});

gulp.task('JSON', function() {
  gulp.src('polyfills/JSON/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(concat('JSON.js'))
    .pipe(size())
    .pipe(gulp.dest('build'));
});

gulp.task('console', function() {
  gulp.src(['polyfills/console/log.js', 'polyfills/console/*.js'])
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(concat('console.js'))
    .pipe(size())
    .pipe(gulp.dest('build'));
});

gulp.task('document.querySelector', function() {
  gulp.src('polyfills/document.querySelector/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(concat('document.querySelector.js'))
    .pipe(size())
    .pipe(gulp.dest('build'));
});

gulp.task('release', ['Object', 'Array', 'Promise', 'String', 'Number', 'JSON', 'console', 'document.querySelector'], function() {
  gulp.src(['build/Object.js', 'build/*.js'])
    .pipe(size())
    .pipe(concat('polyfill.js'))
    .pipe(gulp.dest('dist'));

  gulp.src(['build/Object.js', 'build/*.js'])
    .pipe(uglify())
    .pipe(size())
    .pipe(concat('polyfill.min.js'))
    .pipe(gulp.dest('dist'));
});

gulp.task('default', ['release']);
